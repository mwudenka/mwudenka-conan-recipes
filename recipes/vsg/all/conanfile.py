from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import get, copy
from conan.tools.build import check_min_cppstd
import os

required_conan_version = ">=1.50.0"

class VulkanSceneGraphConan(ConanFile):
    name = "vsg"

    package_type = "library"

    description = "Vulkan & C++17 based Scene Graph Project "
    license = "MIT"
    topics = ("vsg", "vulkan", "graphics", "scenegraph")
    homepage = "https://github.com/vsg-dev/VulkanSceneGraph"
    url = ""
    settings = "os", "arch", "compiler", "build_type"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "with_glsl": [True, False]
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "with_glsl": True
    }

    def export_sources(self):
        copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")
    
    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, 17)

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def requirements(self):
        self.requires("vulkan-loader/1.3.239.0")
        self.requires("vulkan-headers/1.3.239.0", transitive_headers=True)

        if self.options.with_glsl:
            self.requires("glslang/11.7.0", visible=False)
        if self.settings.os == "Linux":
            self.requires("xorg/system", visible=False)

    def layout(self):
        cmake_layout(self)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["VSG_SUPPORTS_ShaderCompiler"] = 0
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE.md", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)

        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none") # Do NOT generate any files
        self.cpp_info.builddirs.append(os.path.join("lib", "cmake", "vsg"))
