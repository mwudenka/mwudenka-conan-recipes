"""
Demonstrates solving a 2D localization problem with SymForce. The goal is for a robot
in a 2D plane to compute its trajectory given distance measurements from wheel odometry
and relative bearing angle measurements to known landmarks in the environment.
"""

import os
import sys
from pathlib import Path
from argparse import ArgumentParser

# -----------------------------------------------------------------------------
# Set the default epsilon to a symbol
# -----------------------------------------------------------------------------
import symforce


symforce.set_epsilon_to_symbol()

# -----------------------------------------------------------------------------
# Create initial Values
# -----------------------------------------------------------------------------
import numpy as np

from symforce import typing as T


# -----------------------------------------------------------------------------
# Define residual functions
# -----------------------------------------------------------------------------
import symforce.symbolic as sf


def bearing_residual(
    pose: sf.Pose2, landmark: sf.V2, angle: sf.Scalar, epsilon: sf.Scalar
) -> sf.V1:
    """
    Residual from a relative bearing measurement of a 2D pose to a landmark.
    """
    t_body = pose.inverse() * landmark
    predicted_angle = sf.atan2(t_body[1], t_body[0], epsilon=epsilon)
    return sf.V1(sf.wrap_angle(predicted_angle - angle))


def odometry_residual(
    pose_a: sf.Pose2, pose_b: sf.Pose2, dist: sf.Scalar, epsilon: sf.Scalar
) -> sf.V1:
    """
    Residual from the scalar distance between two poses.
    """
    return sf.V1((pose_b.t - pose_a.t).norm(epsilon=epsilon) - dist)


def main(output_dir : Path) -> None:
    generate_bearing_residual_code(output_dir)
    generate_odometry_residual_code(output_dir)


# -----------------------------------------------------------------------------
# (Optional) Generate C++ functions for residuals with on-manifold jacobians
# -----------------------------------------------------------------------------
from symforce.codegen import Codegen
from symforce.codegen import CppConfig


def generate_bearing_residual_code(output_dir: Path) -> None:
    """
    Generate C++ code for the bearing residual function. A C++ Factor can then be
    constructed and optimized from this function without any Python dependency.
    """
    # Create a Codegen object for the symbolic residual function, targeted at C++
    codegen = Codegen.function(bearing_residual, config=CppConfig())

    # Generate the function and print the code
    metadata = codegen.generate_function(output_dir=output_dir, skip_directory_nesting=True)

    # Create a Codegen object that computes a linearization from the residual Codegen object,
    # by introspecting and symbolically differentiating the given arguments
    codegen_with_linearization = codegen.with_linearization(which_args=["pose"])

    # Generate the function and print the code
    metadata = codegen_with_linearization.generate_function(
        output_dir=output_dir, skip_directory_nesting=True
    )


def generate_odometry_residual_code(output_dir: Path) -> None:
    """
    Generate C++ code for the odometry residual function. A C++ Factor can then be
    constructed and optimized from this function without any Python dependency.
    """
    # Create a Codegen object for the symbolic residual function, targeted at C++
    codegen = Codegen.function(odometry_residual, config=CppConfig())

    # Generate the function and print the code
    metadata = codegen.generate_function(output_dir=output_dir, skip_directory_nesting=True)

    # Create a Codegen object that computes a linearization from the residual Codegen object,
    # by introspecting and symbolically differentiating the given arguments
    codegen_with_linearization = codegen.with_linearization(which_args=["pose_a", "pose_b"])

    # Generate the function and print the code
    metadata = codegen_with_linearization.generate_function(
        output_dir=output_dir, skip_directory_nesting=True
    )


if __name__ == "__main__":
    argument_parser = ArgumentParser(
        description="Generates c++ code for residuals")
    argument_parser.add_argument(
        "--output_dir",
        type=str,
        help="Path to the root output folder",
        required=True)

    args = argument_parser.parse_args()

    if not os.path.isdir(args.output_dir):
        print(f"Error: {args.output_dir} is not a directory.")
        sys.exit(1)
    output_dir = Path(args.output_dir)

    main(output_dir)
