from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import get, copy, apply_conandata_patches, export_conandata_patches
from conan.tools.build import check_min_cppstd
import os
import sys
import subprocess

required_conan_version = ">=1.50.0"

class SymforceConan(ConanFile):
    name = "symforce"

    package_type = "library"

    description = "Fast symbolic computation, code generation, and nonlinear optimization for robotics"
    license = "Apache-2.0"
    topics = ("robotics", "math", "optimization")
    homepage = "https://symforce.org/"
    url = ""
    settings = "os", "arch", "compiler", "build_type"

    options = {
        "shared": [True, False],
        "fPIC": [True, False]
    }
    default_options = {
        "shared": True,
        "fPIC": True
    }

    def export_sources(self):
        copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")
    
    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, 14)

    def requirements(self):
        self.requires("gmp/6.2.1", visible=False)
        self.requires("spdlog/1.12.0", transitive_headers=True)
        self.requires("tl-optional/1.1.0", transitive_headers=True)
        self.requires("fmt/8.1.1", force=True, transitive_headers=True)
        self.requires("metis/5.2.1", transitive_headers=True)
        self.requires("eigen/3.4.0", transitive_headers=True)

    def configure(self):
        self.options["metis/*"].shared = True
        self.options["metis/*"].with_64bit_types = False

        if self.options.shared:
            self.options.rm_safe("fPIC")

    def build_requirements(self):
        self.test_requires("catch2/3.4.0")

    def layout(self):
        cmake_layout(self)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=True)

    def generate(self):
        apply_conandata_patches(self)
        subprocess.run([sys.executable, '-m', 'pip', 'install', '--user', '-r', 'dev_requirements.txt'], cwd=self.source_folder )
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)
        copy(self, pattern="*", src=os.path.join(self.source_folder, "symforce"), dst=os.path.join(self.package_folder, "python", "symforce"))
        copy(self, pattern="*", src=os.path.join(self.source_folder, "third_party", "skymarshal", "skymarshal"), dst=os.path.join(self.package_folder, "python", "skymarshal"))
        copy(self, pattern="*", src=os.path.join(self.source_folder, "gen", "python", "sym"), dst=os.path.join(self.package_folder, "python", "sym"))
        copy(self, pattern="*", src=os.path.join(self.build_folder, "lcmtypes", "python2.7", "lcmtypes"), dst=os.path.join(self.package_folder, "python", "lcmtypes"))
        copy(self, pattern="*", src=os.path.join(self.build_folder, "pybind"), dst=os.path.join(self.package_folder, "python"))

        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        python_folder = os.path.join(self.package_folder, "python")
        
        self.buildenv_info.prepend_path("PYTHONPATH", python_folder)
        self.runenv_info.prepend_path("PYTHONPATH", python_folder)

        self.cpp_info.libs = ["symforce_cholesky", "symforce_gen", "symforce_opt", "symforce_slam"]
