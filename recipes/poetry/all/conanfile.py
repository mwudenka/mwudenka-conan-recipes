import os
import stat
from conan import ConanFile
from conan.tools.files import copy, get


class PoetryRecipe(ConanFile):
    name = "poetry"
    package_type = "application"

    description = ""
    license = "MIT"
    topics = ("python", "package manager")
    homepage = "https://python-poetry.org/"
    url = ""

    # Binary configuration
    settings = ()

    exports_sources = "install_poetry.py"

    def layout(self):
        pass

    def build_requirements(self):
        self.tool_requires("cpython/[>=3.8 <3.13]")

    def source(self):
        pass

    def package(self):
        self.run(f"POETRY_HOME={self.package_folder} POETRY_VERSION={self.version} python3 install_poetry.py", cwd=self.source_folder)

    def package_info(self):
        self.cpp_info.frameworkdirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.resdirs = []
        self.cpp_info.includedirs = []

        self.runenv_info.prepend_path("PATH", self.package_folder)

        # ending = ".bat" if self.settings.os == "Windows" else ".sh"
        # exec = os.path.join(self.package_folder, "res", f"structurizr{ending}")
        # self.runenv_info.prepend_path("CLASSPATH", exec)