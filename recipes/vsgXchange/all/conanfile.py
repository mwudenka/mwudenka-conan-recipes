from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import get, copy, apply_conandata_patches, export_conandata_patches
from conan.tools.build import check_min_cppstd
import os

required_conan_version = ">=1.50.0"

class VsgImGuiConan(ConanFile):
    name = "vsgxchange"
    description = "Utility library for converting data+materials to/from VulkanSceneGraph"
    license = "MIT"
    topics = ("vsg", "vulkan", "png", "jpg", "ktx", "dds", "gif", "bmp", "tga", "psd")
    homepage = "https://github.com/vsg-dev/vsgXchange"
    url = ""
    settings = "os", "arch", "compiler", "build_type"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "with_assimp": [True, False],
        "with_curl": [True, False],
        "with_freetype": [True, False],
        "with_gdal": [True, False],
        "with_openexr": [True, False]
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "with_assimp": False,
        "with_curl": False,
        "with_freetype": False,
        "with_gdal": False,
        "with_openexr": False
    }

    def export_sources(self):
        copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, 17)

    def requirements(self):
        self.requires("vsg/1.1.4@mwudenka/snapshot", transitive_headers=True)

        if self.options.with_assimp:
            self.requires("assimp/5.4.1")
        if self.options.with_curl:
            self.requires("libcurl/8.8.0")
        if self.options.with_freetype:
            self.requires("freetype/2.13.2")
        if self.options.with_gdal:
            self.requires("gdal/3.8.8", transitive_headers=True)
            self.requires("zlib/1.3.1") # dependency override
            self.requires("libpng/1.6.43") # dependency override
        if self.options.with_openexr:
            self.requires("openexr/3.2.4")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")

    def layout(self):
        #cmake_layout(self, src_folder="src")
        cmake_layout(self)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=True)

    def generate(self):
        tc = CMakeToolchain(self)
        tc.variables["vsgXchange_assimp"] = self.options.with_assimp
        tc.variables["vsgXchange_CURL"] = self.options.with_curl
        tc.variables["vsgXchange_freetype"] = self.options.with_freetype
        tc.variables["vsgXchange_GDAL"] = self.options.with_gdal
        tc.variables["vsgXchange_OpenEXR"] = self.options.with_openexr
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE.txt", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)

        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none") # Do NOT generate any files
        self.cpp_info.builddirs.append(os.path.join("lib", "cmake", "vsgXchange"))
