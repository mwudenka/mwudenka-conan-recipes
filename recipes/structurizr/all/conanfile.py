import os
import stat
from conan import ConanFile
from conan.tools.files import copy, get


class structurizrRecipe(ConanFile):
    name = "structurizr"
    package_type = "application"

    description = "The Structurizr DSL provides a way to define a software architecture model (based upon the C4 model) using a text-based domain specific language (DSL)."
    license = "Apache-2.0"
    topics = ("modeling")
    homepage = "https://structurizr.com/"
    url = "https://github.com/structurizr/cli"

    # Binary configuration
    settings = "os"

    def layout(self):
        pass

    def requirements(self):
        self.requires("openjdk/21.0.1")

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=False)

    def package(self):
        if self.settings.os == "Windows":
            copy(self, pattern="structurizr.bat", dst=self.package_folder, src=self.source_folder)
        else:
            copy(self, pattern="structurizr.sh", dst=self.package_folder, src=self.source_folder)
            file = os.path.join(self.package_folder, "structurizr.sh")
            st = os.stat(file)
            os.chmod(file, st.st_mode | stat.S_IEXEC)

        copy(self, pattern="*", dst=os.path.join(self.package_folder, "lib"), src=os.path.join(self.source_folder, "lib"))

    def package_info(self):
        self.cpp_info.frameworkdirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.resdirs = []
        self.cpp_info.includedirs = []

        self.runenv_info.prepend_path("PATH", self.package_folder)

        # ending = ".bat" if self.settings.os == "Windows" else ".sh"
        # exec = os.path.join(self.package_folder, "res", f"structurizr{ending}")
        # self.runenv_info.prepend_path("CLASSPATH", exec)