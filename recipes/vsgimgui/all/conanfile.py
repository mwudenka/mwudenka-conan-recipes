from conan import ConanFile
from conan.tools.cmake import CMake, CMakeDeps, CMakeToolchain, cmake_layout
from conan.tools.files import get, copy, apply_conandata_patches, export_conandata_patches
from conan.tools.build import check_min_cppstd
from conan.tools.scm import Version
import os

required_conan_version = ">=1.50.0"

class VsgImGuiConan(ConanFile):
    name = "vsgimgui"

    package_type = "library"

    description = "Integration of VulkanSceneGraph with ImGui"
    license = "MIT"
    topics = ("vsg", "vulkan", "gui", "imgui")
    homepage = "https://github.com/vsg-dev/vsgImGui"
    url = ""
    settings = "os", "arch", "compiler", "build_type"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
    }

    def requirements(self):
        if Version(self.version) >= "0.4.0":
            self.requires("imgui/1.90.8", transitive_headers=True, force=True) # force override implots dependency on imgui
            self.requires("implot/0.16", transitive_headers=True)
            self.requires("vsg/1.1.4@mwudenka/snapshot", transitive_headers=True)
        elif Version(self.version) >= "0.2.0":
            self.requires("imgui/1.89.8", transitive_headers=True, force=True) # force override implots dependency on imgui
            self.requires("implot/0.15", transitive_headers=True)
            self.requires("vsg/1.1.4@mwudenka/snapshot", transitive_headers=True)
        elif Version(self.version) >= "0.1.0":
            self.requires("imgui/1.89.7", transitive_headers=True, force=True) # force override implots dependency on imgui
            self.requires("implot/0.14", transitive_headers=True)
            self.requires("vsg/1.1.2@mwudenka/snapshot", transitive_headers=True)
        else:
            self.requires("imgui/1.89.4", transitive_headers=True, force=True) # force override implots dependency on imgui
            self.requires("implot/0.14", transitive_headers=True)
            self.requires("vsg/1.0.5@mwudenka/snapshot", transitive_headers=True)

    def export_sources(self):
        copy(self, "CMakeLists.txt", self.recipe_folder, self.export_sources_folder)
        export_conandata_patches(self)

    def config_options(self):
        if self.settings.os == "Windows":
            self.options.rm_safe("fPIC")

    def configure(self):
        if self.options.shared:
            self.options.rm_safe("fPIC")
    
    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, 17)

    def layout(self):
        cmake_layout(self)

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=True)

    def generate(self):
        copy(self, "imgui_impl_vulkan.h", self.dependencies["imgui"].cpp_info.srcdirs[0], os.path.join(self.source_folder, "src", "imgui", "backends"))
        copy(self, "imgui_impl_vulkan.cpp", self.dependencies["imgui"].cpp_info.srcdirs[0], os.path.join(self.source_folder, "src", "imgui", "backends"))
        os.remove(os.path.join(self.source_folder, "include", "vsgImGui", "imgui.h"))
        os.remove(os.path.join(self.source_folder, "include", "vsgImGui", "implot.h"))
        tc = CMakeToolchain(self)
        tc.variables["SHOW_DEMO_WINDOW"] = "OFF"
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def build(self):
        apply_conandata_patches(self)
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        copy(self, pattern="LICENSE.txt", dst=os.path.join(self.package_folder, "licenses"), src=self.source_folder)

        copy(self, pattern="*.h",
            dst=os.path.join(self.package_folder, "include"),
            src=os.path.join(self.source_folder, "include"))
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_find_mode", "none") # Do NOT generate any files
        self.cpp_info.builddirs.append(os.path.join("lib", "cmake", "vsgImGui"))
