#include <cassert>
#include <crill/reclaim_object.h>

#include <iostream>

int main(int argc, char **argv) {
  struct test_t {
    test_t() : i(42) {}
    int i;
  };

  crill::reclaim_object<test_t> obj;
  auto reader = obj.get_reader();
  std::cout << reader.get_value().i << '\n';

  return 0;
}