from conan import ConanFile
from conan.tools.files import get
from conan.tools.build import check_min_cppstd
from conan.tools.files import copy
import os

required_conan_version = ">=1.50.0"

class CrillConan(ConanFile):
    name = "crill"

    package_type = "header-library"

    description = "Cross-platform Real-time, I/O, and Low-latency Library"
    license = "BSL-1.0"
    topics = ("rcu", "mutex", "threading")
    homepage = "https://github.com/crill-dev/crill"
    url = ""

    exports_sources = "include/*"
    no_copy_source = True

    def source(self):
        get(self, **self.conan_data["sources"][self.version], destination=self.source_folder, strip_root=True)

    def package(self):
        copy(
            self,
            "*.h",
            self.source_folder,
            self.package_folder
        )
