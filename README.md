# My Conan Recipes

## Usage

Add my conan remote:

```sh
conan remote add mwudenka https://conan2.wudenka.de
```

List all available packages:

```sh
conan search "*" -r=mwudenka
```
